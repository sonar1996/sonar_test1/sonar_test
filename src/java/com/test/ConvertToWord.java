/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.test;

/**
 *
 * @author akashkolte
 */
public class ConvertToWord {
    
    public ConvertToWord()
    {
        s = "";
        inpstr = "";
        temp = "";
    }

    public  String fun2(double xDouble)
    {
        int x = (int)xDouble;
        inpstr = (new StringBuilder()).append("").append(x).toString();
        inpstr = inpstr.trim();
        inputLength = inpstr.length();
        s = "";
        fun1(inputLength);
        return s;
    }

    public void fun1(int x)
    {
        if(x == 1)
            ones(inpstr);
        else
        if(x == 2)
            tens(inpstr);
        else
        if(x == 3)
        {
            ones(inpstr.substring(0, 1));
            if(!"0".equals(inpstr.substring(0, 1)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(1, 3));
        } else
        if(x == 4)
        {
            ones(inpstr.substring(0, 1));
            if(!"0".equals(inpstr.substring(0, 1)))
                s = (new StringBuilder()).append(s).append(" thousand").toString();
            ones(inpstr.substring(1, 2));
            if(!"0".equals(inpstr.substring(1, 2)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(2, 4));
        } else
        if(x == 5)
        {
            tens(inpstr.substring(0, 2));
            if(!"00".equals(inpstr.substring(0, 2)))
                s = (new StringBuilder()).append(s).append(" thousand").toString();
            ones(inpstr.substring(2, 3));
            if(!"0".equals(inpstr.substring(2, 3)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(3, 5));
        } else
        if(x == 6)
        {
            ones(inpstr.substring(0, 1));
            if(!"0".equals(inpstr.substring(0, 1)))
                s = (new StringBuilder()).append(s).append(" lakh").toString();
            tens(inpstr.substring(1, 3));
            if(!"00".equals(inpstr.substring(1, 3)))
                s = (new StringBuilder()).append(s).append(" thousand").toString();
            ones(inpstr.substring(3, 4));
            if(!"0".equals(inpstr.substring(3, 4)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(4, 6));
        } else
        if(x == 7)
        {
            tens(inpstr.substring(0, 2));
            if(!"00".equals(inpstr.substring(0, 2)))
                s = (new StringBuilder()).append(s).append(" lakh").toString();
            tens(inpstr.substring(2, 4));
            if(!"00".equals(inpstr.substring(2, 4)))
                s = (new StringBuilder()).append(s).append(" thousand").toString();
            ones(inpstr.substring(4, 5));
            if(!"0".equals(inpstr.substring(4, 5)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(5, 7));
        }
        
        else if(x == 8)
        {
            ones(inpstr.substring(0, 1));
            if(!"0".equals(inpstr.substring(0, 1)))
                s = (new StringBuilder()).append(s).append(" crore").toString();
            tens(inpstr.substring(1, 3));
            if(!"00".equals(inpstr.substring(1, 3)))
                s = (new StringBuilder()).append(s).append(" thousand").toString();
            ones(inpstr.substring(3, 4));
            if(!"0".equals(inpstr.substring(3, 4)))
                s = (new StringBuilder()).append(s).append(" hundred").toString();
            tens(inpstr.substring(4, 6));
        }
    }

    public void tens2(String x)
    {
        temp = x;
        x = x.substring(0, 1);
        if("2".equals(x))
            s = (new StringBuilder()).append(s).append(" twenty").toString();
        else
        if("3".equals(x))
            s = (new StringBuilder()).append(s).append(" thirty").toString();
        else
        if("4".equals(x))
            s = (new StringBuilder()).append(s).append(" fourty").toString();
        else
        if("50".equals(x))
            s = (new StringBuilder()).append(s).append(" fifty").toString();
        else
        if("6".equals(x))
            s = (new StringBuilder()).append(s).append(" sixty").toString();
        else
        if("7".equals(x))
            s = (new StringBuilder()).append(s).append(" seventy").toString();
        else
        if("8".equals(x))
            s = (new StringBuilder()).append(s).append(" eighty").toString();
        else
        if("9".equals(x))
            s = (new StringBuilder()).append(s).append(" ninty").toString();
        ones(temp.substring(1, 2));
    }

    public void tens(String x)
    {
        if("10".equals(x))
            s = (new StringBuilder()).append(s).append(" ten").toString();
        else
        if("20".equals(x))
            s = (new StringBuilder()).append(s).append(" twenty").toString();
        else
        if("30".equals(x))
            s = (new StringBuilder()).append(s).append(" thirty").toString();
        else
        if("40".equals(x))
            s = (new StringBuilder()).append(s).append(" fourty").toString();
        else
        if("50".equals(x))
            s = (new StringBuilder()).append(s).append(" fifty").toString();
        else
        if("60".equals(x))
            s = (new StringBuilder()).append(s).append(" sixty").toString();
        else
        if("70".equals(x))
            s = (new StringBuilder()).append(s).append(" seventy").toString();
        else
        if("80".equals(x))
            s = (new StringBuilder()).append(s).append(" eighty").toString();
        else
        if("90".equals(x))
            s = (new StringBuilder()).append(s).append(" ninty").toString();
        else
        if("11".equals(x))
            s = (new StringBuilder()).append(s).append(" eleven").toString();
        else
        if("12".equals(x))
            s = (new StringBuilder()).append(s).append(" twelve").toString();
        else
        if("13".equals(x))
            s = (new StringBuilder()).append(s).append(" thirteen").toString();
        else
        if("14".equals(x))
            s = (new StringBuilder()).append(s).append(" fourteen").toString();
        else
        if("15".equals(x))
            s = (new StringBuilder()).append(s).append(" fifteen").toString();
        else
        if("16".equals(x))
            s = (new StringBuilder()).append(s).append(" sixteen").toString();
        else
        if("17".equals(x))
            s = (new StringBuilder()).append(s).append(" seventeen").toString();
        else
        if("18".equals(x))
            s = (new StringBuilder()).append(s).append(" eighteen").toString();
        else
        if("19".equals(x))
            s = (new StringBuilder()).append(s).append(" ninteen").toString();
        else
            tens2(x);
    }

    public void ones(String x)
    {
        if("1".equals(x))
            s = (new StringBuilder()).append(s).append(" one").toString();
        else
        if("2".equals(x))
            s = (new StringBuilder()).append(s).append(" two").toString();
        else
        if("3".equals(x))
            s = (new StringBuilder()).append(s).append(" three").toString();
        else
        if("4".equals(x))
            s = (new StringBuilder()).append(s).append(" four").toString();
        else
        if("5".equals(x))
            s = (new StringBuilder()).append(s).append(" five").toString();
        else
        if("6".equals(x))
            s = (new StringBuilder()).append(s).append(" six").toString();
        else
        if("7".equals(x))
            s = (new StringBuilder()).append(s).append(" seven").toString();
        else
        if("8".equals(x))
            s = (new StringBuilder()).append(s).append(" eight").toString();
        else
        if("9".equals(x))
            s = (new StringBuilder()).append(s).append(" nine").toString();
    }

    String s;
    String inpstr;
    int inputLength;
    String temp;
    
    public static void main(String args[]){
        ConvertToWord obj=new ConvertToWord();
        System.out.println("message: "+obj.fun2(1223)); 
    }
}
